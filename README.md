# Same Ol' Snakes

## Desenvolvimento:
Jogo estilo snake feito no Unity com o intúito de aprender a programar melhor na game engine.
Todas as imagens e sons foram feitos por mim

## Link do gameplay do jogo:
[![Link do gameplay do jogo Same Old Snakes](Assets/Thumbnails/same-ol-snakes-gameplay-thumbnail.webp)](https://youtu.be/xjka7hLPM-M)
