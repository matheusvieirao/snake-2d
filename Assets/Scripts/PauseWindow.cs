using CodeMonkey.Utils;
using UnityEngine;

public class PauseWindow : MonoBehaviour {
    private static PauseWindow instance;
    private void Awake() {
        instance = this;
        transform.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        transform.Find("returnBtn").GetComponent<Button_UI>().ClickFunc = () => GameHandler.ResumeGame();
        transform.Find("returnBtn").GetComponent<Button_UI>().AddButtonSound(SoundManager.Sound.ButtonClick);
        transform.Find("mainMenuBtn").GetComponent<Button_UI>().ClickFunc = () => Loader.Load(Loader.Scene.MainMenu);
        transform.Find("mainMenuBtn").GetComponent<Button_UI>().AddButtonSound(SoundManager.Sound.ButtonClick);

        Hide();
    }
    public static void Show() {
        instance.gameObject.SetActive(true);
    }
    public static void Hide() {
        instance.gameObject.SetActive(false);
    }

}
