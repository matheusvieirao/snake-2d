using CodeMonkey.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SoundManager {

    public enum Sound {
        SnakeDie,
        SnakeEat,
        ButtonClick
    }

    public static void PlaySound(Sound sound) {
        GameObject soundGameObject = new GameObject("Sound");
        AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();
        audioSource.PlayOneShot(GetAudioClipe(sound));
    }

    private static AudioClip GetAudioClipe(Sound sound) {
        List<int> indexList = new List<int>();
        
        for (int i=0; i< GameAssets.instance.soundAudioClips.Length; i++) {
            if (GameAssets.instance.soundAudioClips[i].sound == sound) {
                indexList.Add(i);
            }
        }

        if (indexList.Count > 0) {
            int randomIndex = indexList[Random.Range(0, indexList.Count)];
            return GameAssets.instance.soundAudioClips[randomIndex].audioClip;
        }
        else {
            Debug.LogError("Sound " + sound + " not found");
            return null;
        }
    }

    public static void AddButtonSound(this Button_UI buttonUI, Sound sound) {
        buttonUI.ClickFunc += () => SoundManager.PlaySound(sound);
    }

}
