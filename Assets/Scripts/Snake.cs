﻿using CodeMonkey.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour
{
    private enum State { Alive, Dead }
    private State state;
    private Vector2Int gridMoveDirection;
    private Vector2Int gridMoveDirectionAtual;
    private Vector2Int headPosition;
    private float gridMoveTimer;
    private float gridMoveTimerMax;
    private LevelGrid levelGrid;
    private List<Vector2Int> snakeBodyPartPositionList;
    private List<Vector2Int> snakeBodyPartDirectionList;
    private List<SnakeBodyPart> snakeBodyPartList;

    public void Setup(LevelGrid levelGrid) {
        this.levelGrid = levelGrid;
    }

    private void Awake() {
        headPosition = new Vector2Int(5, 5);    
        gridMoveTimerMax = .2f; //velocidade
        gridMoveTimer = gridMoveTimerMax;
        gridMoveDirection = new Vector2Int(0, 1);
        gridMoveDirectionAtual = new Vector2Int(0, 1);

        snakeBodyPartPositionList = new List<Vector2Int>();
        snakeBodyPartDirectionList = new List<Vector2Int>();

        snakeBodyPartList = new List<SnakeBodyPart>();
        GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeHeadSprite;

        state = State.Alive;
    }

    private void Update() {
        switch (state) {
            case State.Alive:
                HandleInput();
                HandleGridMovement();
                break;
            case State.Dead:
                break;
        }
    }

    private void HandleInput() {
        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            MoveUp();
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            MoveDown();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MoveRight();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            MoveLeft();
        }
    }

    public void MoveUp()
    {
        if (gridMoveDirectionAtual.y != -1)
        {
            gridMoveDirection.x = 0;
            gridMoveDirection.y = 1;
        }
    }

    public void MoveDown()
    {
        if (gridMoveDirectionAtual.y != +1)
        {
            gridMoveDirection.x = 0;
            gridMoveDirection.y = -1;
        }
    }

    public void MoveRight()
    {
        if (gridMoveDirectionAtual.x != -1)
        {
            gridMoveDirection.x = 1;
            gridMoveDirection.y = 0;
        }
    }

    public void MoveLeft()
    {
        if (gridMoveDirectionAtual.x != +1)
        {
            gridMoveDirection.x = -1;
            gridMoveDirection.y = 0;
        }
    }

    private void HandleGridMovement() {
        gridMoveTimer += Time.deltaTime;
        if (gridMoveTimer >= gridMoveTimerMax) {
            gridMoveTimer -= gridMoveTimerMax;
            gridMoveDirectionAtual = gridMoveDirection; //so the snake doesn't go down if it is going up

            snakeBodyPartPositionList.Add(headPosition);
            snakeBodyPartDirectionList.Add(gridMoveDirectionAtual);
            headPosition += gridMoveDirection;
            headPosition = levelGrid.InfiniteEdges(headPosition);


            foreach (SnakeBodyPart snakeBodyPart in snakeBodyPartList) {
                if (headPosition == snakeBodyPart.GetPosition()) {
                    state = State.Dead;
                    SoundManager.PlaySound(SoundManager.Sound.SnakeDie);
                    GameHandler.SnakeDied();
                }
            }

            if (state == State.Alive) {
                MoveHead();
                if (levelGrid.SnakeAteFood(headPosition)) {
                    SoundManager.PlaySound(SoundManager.Sound.SnakeEat);
            Score.AddScore();
                    IncreaseBodySize();
                }
                else {
                    RemoveLastBodyPart();
                }
                UpdateSnakeBodyParts();
            }

        }
    }

    private void MoveHead() {
        transform.position = new Vector3(headPosition.x, headPosition.y);
        transform.eulerAngles = new Vector3(0, 0, GetAngleFromVector(gridMoveDirection) - 90);
    }

    private void RemoveLastBodyPart() {
        snakeBodyPartPositionList.RemoveAt(0);
        snakeBodyPartDirectionList.RemoveAt(0);
    }

    private void UpdateSnakeBodyParts() {

        for (int i = 0; i < snakeBodyPartList.Count; i++) {
            snakeBodyPartList[i].SetBodyPosition(snakeBodyPartPositionList[i]);
            snakeBodyPartList[i].SetBodyDirection(snakeBodyPartDirectionList[i]);
            if (i>0) {
                snakeBodyPartList[i].SetBodySprite(snakeBodyPartList[i-1].GetBodyDirection());
            }
            // cauda, ultima parte do corpo
            else {
                snakeBodyPartList[i].SetBodySprite(new Vector2Int(0, 0));
            }
        }
    }

    private float GetAngleFromVector(Vector2Int dir) {
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (n < 0) n += 360;
        return n;
    }

    // Return Head + Body
    public List<Vector2Int> GetFullSnakeGridPositionList() {
        List<Vector2Int> headPositionList = new List<Vector2Int>() { };
        headPositionList.Add(headPosition);
        headPositionList.AddRange(snakeBodyPartPositionList);
        return headPositionList;
    }

    public void IncreaseBodySize() {
        snakeBodyPartList.Add(new SnakeBodyPart(snakeBodyPartList.Count));
    }


    private class SnakeBodyPart {
        private Vector2Int bodydirection;
        private Vector2Int bodyPosition;
        GameObject snakeBodyGameObject;

        public SnakeBodyPart(int bodyIndex) {

            snakeBodyGameObject = new GameObject("SnakeBody", typeof(SpriteRenderer));
            //ordena para nao renderizar uma em cima da outra de mandeira errada
            snakeBodyGameObject.GetComponent<SpriteRenderer>().sortingOrder = bodyIndex;
            snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyISprite;

        }

        public void SetBodyPosition(Vector2Int bodyPosition) {
            this.bodyPosition = bodyPosition;
            snakeBodyGameObject.transform.position = new Vector3(bodyPosition.x, bodyPosition.y);
        }
        public void SetBodyDirection(Vector2Int bodydirection) {
            this.bodydirection = bodydirection;
        }
        public Vector2Int GetBodyDirection() {
            return bodydirection;
        }
        public void SetBodySprite(Vector2Int movAnterior) {
            Vector2Int direita = new Vector2Int(1, 0);
            Vector2Int esquerda = new Vector2Int(-1, 0);
            Vector2Int cima = new Vector2Int(0, 1);
            Vector2Int baixo = new Vector2Int(0, -1);
            Vector2Int zeroZero = new Vector2Int(0, 0);

            // pra baixo + direita
            if (movAnterior == baixo && bodydirection == direita) {
                snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyLSprite;
                snakeBodyGameObject.GetComponent<SpriteRenderer>().flipX = false;
                snakeBodyGameObject.transform.eulerAngles = new Vector3(0, 0, 0);
            }
            // pra direita + cima
            else if (movAnterior == direita && bodydirection == cima) {
                snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyLSprite;
                snakeBodyGameObject.GetComponent<SpriteRenderer>().flipX = false;
                snakeBodyGameObject.transform.eulerAngles = new Vector3(0, 0, 90);
            }
            // pra cima + esquerda
            else if (movAnterior == cima && bodydirection == esquerda) {
                snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyLSprite;
                snakeBodyGameObject.GetComponent<SpriteRenderer>().flipX = false;
                snakeBodyGameObject.transform.eulerAngles = new Vector3(0, 0, 180);
            }
            // pra esquerda + baixo
            else if (movAnterior == esquerda && bodydirection == baixo) {
                snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyLSprite;
                snakeBodyGameObject.GetComponent<SpriteRenderer>().flipX = false;
                snakeBodyGameObject.transform.eulerAngles = new Vector3(0, 0, 270);
            }
            // pra baixo + esquerda //////////////
            else if (movAnterior == baixo && bodydirection == esquerda) {
                snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyLSprite;
                snakeBodyGameObject.GetComponent<SpriteRenderer>().flipX = true;
                snakeBodyGameObject.transform.eulerAngles = new Vector3(0, 0, 0);
            }
            // pra esquerda + cima
            else if (movAnterior == esquerda && bodydirection == cima) {
                snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyLSprite;
                snakeBodyGameObject.GetComponent<SpriteRenderer>().flipX = true;
                snakeBodyGameObject.transform.eulerAngles = new Vector3(0, 0, 270);
            }
            // pra cima + direita
            else if (movAnterior == cima && bodydirection == direita) {
                snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyLSprite;
                snakeBodyGameObject.GetComponent<SpriteRenderer>().flipX = true;
                snakeBodyGameObject.transform.eulerAngles = new Vector3(0, 0, 180);
            }
            // pra direita + baixo
            else if (movAnterior == direita && bodydirection == baixo) {
                snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyLSprite;
                snakeBodyGameObject.GetComponent<SpriteRenderer>().flipX = true;
                snakeBodyGameObject.transform.eulerAngles = new Vector3(0, 0, 90);
            }
            // direita
            else if (bodydirection == direita) {
                if (movAnterior == zeroZero)
                    snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyCaudaSprite;
                else
                    snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyISprite;
                snakeBodyGameObject.GetComponent<SpriteRenderer>().flipX = false;
                snakeBodyGameObject.transform.eulerAngles = new Vector3(0, 0, 0);
            }
            // esquerda
            else if (bodydirection == esquerda) {
                if (movAnterior == zeroZero)
                    snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyCaudaSprite;
                else
                    snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyISprite;
                snakeBodyGameObject.GetComponent<SpriteRenderer>().flipX = false;
                snakeBodyGameObject.transform.eulerAngles = new Vector3(0, 0, 180);
            }
            // cima
            else if (bodydirection == cima) {
                if (movAnterior == zeroZero)
                    snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyCaudaSprite;
                else
                    snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyISprite;
                snakeBodyGameObject.GetComponent<SpriteRenderer>().flipX = false;
                snakeBodyGameObject.transform.eulerAngles = new Vector3(0, 0, 90);
            }
            // baixo
            else if (bodydirection == baixo) {
                if (movAnterior == zeroZero)
                    snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyCaudaSprite;
                else
                    snakeBodyGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodyISprite;
                snakeBodyGameObject.GetComponent<SpriteRenderer>().flipX = false;
                snakeBodyGameObject.transform.eulerAngles = new Vector3(0, 0, 270);
            }
        }

        internal Vector2Int GetPosition() {
            return bodyPosition;
        }
    }
}
