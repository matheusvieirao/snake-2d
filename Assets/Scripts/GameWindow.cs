﻿using CodeMonkey.Utils;
using UnityEngine;
using UnityEngine.UI;

public class GameWindow : MonoBehaviour
{
    private Text scoreText;
    private Snake snake;

    private void Awake() {
        transform.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        snake = GameObject.FindWithTag("Snake").GetComponent<Snake>();
        transform.Find("pauseBtn").GetComponent<Button_UI>().ClickFunc = () => GameHandler.PauseGame();
        transform.Find("pauseBtn").GetComponent<Button_UI>().AddButtonSound(SoundManager.Sound.ButtonClick);
        transform.Find("upBtn").GetComponent<Button_UI>().MouseDownOnceFunc = () => snake.MoveUp();
        transform.Find("downBtn").GetComponent<Button_UI>().MouseDownOnceFunc = () => snake.MoveDown();
        transform.Find("rightBtn").GetComponent<Button_UI>().MouseDownOnceFunc = () => snake.MoveRight();
        transform.Find("leftBtn").GetComponent<Button_UI>().MouseDownOnceFunc = () => snake.MoveLeft();

        scoreText = transform.Find("ScoreText").GetComponent<Text>();

        Score.OnHighscoreChanged += Score_OnHighscoreChanged;
        UpdateHighscore();
    }
    public void Setup(Snake snake)
    {
        this.snake = snake;
    }

    private void Score_OnHighscoreChanged(object sender, System.EventArgs e) {
        UpdateHighscore();
    }

    // Update is called once per frame
    void Update() {
        scoreText.text = Score.GetScore().ToString();
    }

    private void UpdateHighscore() {
        int highscore = Score.GetHighScore();
        transform.Find("highscoreText").GetComponent<Text>().text = "Highscore: " + highscore.ToString();
    }
}
