﻿
using UnityEngine;

public class GameHandler : MonoBehaviour {
    public Snake snake;
    private LevelGrid levelGrid;

    private void Awake() {
        Score.InitializeStatic();
        Time.timeScale = 1f;
    }

    private void Start()
    {
        levelGrid = new LevelGrid(10, 10);

        levelGrid.Setup(snake);
        snake.Setup(levelGrid);

        levelGrid.SpawnFood();
        ResumeGame();
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.P)) {
            if (IsGamePaused()) {
                ResumeGame();
            }
            else {
                PauseGame();
            }
        }
    }

    public static void SnakeDied() {
        Score.TrySetNewHighscore();
        GameAssets.instance.GetComponent<AudioSource>().Stop(); //para a trilha sonora
        GameOverWindow.Show();
    }
    internal static void ResumeGame() {
        PauseWindow.Hide();
        Time.timeScale = 1f;
    }

    public static void PauseGame() {
        PauseWindow.Show();
        Time.timeScale = 0f;
    }

    public static bool IsGamePaused() {
        return Time.timeScale == 0f;
    }
}
