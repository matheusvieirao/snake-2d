using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;
using System;

public class MainMenu : MonoBehaviour {
    private enum Sub {
        Main,
        LevelSelection
    }

    private void Awake() {
        transform.Find("levelSelectionSub").GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        // main menu
        transform.Find("mainSub").Find("playBtn").GetComponent<Button_UI>().ClickFunc = () => Loader.Load(Loader.Scene.GameScene);
        transform.Find("mainSub").Find("playBtn").GetComponent<Button_UI>().AddButtonSound(SoundManager.Sound.ButtonClick);
        transform.Find("mainSub").Find("levelsBtn").GetComponent<Button_UI>().ClickFunc = () => ShowSub(Sub.LevelSelection);
        transform.Find("mainSub").Find("levelsBtn").GetComponent<Button_UI>().AddButtonSound(SoundManager.Sound.ButtonClick);

        // level selection menu
        transform.Find("levelSelectionSub").Find("backBtn").GetComponent<Button_UI>().ClickFunc = () => ShowSub(Sub.Main);
        transform.Find("levelSelectionSub").Find("backBtn").GetComponent<Button_UI>().AddButtonSound(SoundManager.Sound.ButtonClick);

        ShowSub(Sub.Main);
    }

    private void ShowSub(Sub sub) {
        transform.Find("mainSub").gameObject.SetActive(false);
        transform.Find("levelSelectionSub").gameObject.SetActive(false);

        switch (sub) {
            case Sub.Main:
                transform.Find("mainSub").gameObject.SetActive(true);
                break;
            case Sub.LevelSelection:
                transform.Find("levelSelectionSub").gameObject.SetActive(true);
                break;
        }
    }
}
