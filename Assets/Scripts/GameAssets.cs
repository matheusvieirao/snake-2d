﻿using System;
using UnityEngine;

public class GameAssets : MonoBehaviour {
    public static GameAssets instance;
    AudioSource audioSource;

    private void Awake() {
        instance = this;
    }

    public Sprite snakeHeadSprite;
    public Sprite foodSprite;
    public Sprite snakeBodyLSprite;
    public Sprite snakeBodyISprite;
    public Sprite snakeBodyCaudaSprite;
    public SoundAudioClip[] soundAudioClips;

    [Serializable]
    public class SoundAudioClip {
        public SoundManager.Sound sound;
        public AudioClip audioClip;
    }
}
