﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGrid
{
    private GameObject foodGameObject;
    private Vector2Int foodGridPosition;
    private int width;
    private int height;
    private Snake snake;

    public LevelGrid(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void Setup(Snake snake) {
        this.snake = snake;
    }

    public void SpawnFood() {
        do {
            foodGridPosition = new Vector2Int(Random.Range(0, width), Random.Range(0, height));
        } while (snake.GetFullSnakeGridPositionList().IndexOf(foodGridPosition) != -1);

        foodGameObject = new GameObject("Food", typeof(SpriteRenderer));
        foodGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.foodSprite;
        foodGameObject.transform.position = new Vector3(foodGridPosition.x, foodGridPosition.y);
    }

    public bool SnakeAteFood(Vector2Int snakeGridPostion) {
        
        if (snakeGridPostion == foodGridPosition) {
            Object.Destroy(foodGameObject);
            SpawnFood();
            return true;
        }
        else {
            return false;
        }
        
    }


    internal Vector2Int InfiniteEdges(Vector2Int headPosition) {
        if (headPosition.x < 0) {
            headPosition.x = width - 1;
        }
        if (headPosition.y < 0) {
            headPosition.y = height - 1;
        }
        if (headPosition.x >= width) {
            headPosition.x = 0;
        }
        if (headPosition.y >= height) {
            headPosition.y = 0;
        }
        return headPosition;
    }
}
