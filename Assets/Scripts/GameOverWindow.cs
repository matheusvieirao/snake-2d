﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;

public class GameOverWindow : MonoBehaviour {
    private static GameOverWindow instance;

    private void Awake () {
        instance = this;
        transform.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        transform.Find("retryBtn").GetComponent<Button_UI>().ClickFunc = () => {
            Loader.Load(Loader.Scene.GameScene);
        };
        transform.Find("retryBtn").GetComponent<Button_UI>().AddButtonSound(SoundManager.Sound.ButtonClick);
        gameObject.SetActive(false);
    }

    public static void Show() {
        instance.gameObject.SetActive(true);
    }

}
